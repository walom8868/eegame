### Group ###
Roberto Arias-Yacupoma(rga34), Qi Dong(qd33), Louis Katchen(lhk31)

### How do I get set up? ###

Run server from Build folder of repository (EEGame/Build) :
python -m SimpleHTTPServer 8000
or if using python 3:
python -m http.server 8000

Then visit:
http://localhost:8000/HauntedPartyTime.html

### Overview of Source Files ###

1. EEGame / Assets Directory: Unity Source Code for Haunted Party Time game


2. EEGame / Build Directory: Contains all files for web playable version of Haunted Party Time game


  2.1 EEGame / Build / bootstrap: Bootstrap framework files used for website styling ( http://getbootstrap.com/ )

  2.2 EEGame / Build / images: Emoji of social states for update box
  
  2.3 EEGame / Build / img: Sprite images for character selection buttons
  
  2.4 EEGame / Build / newdata / data: Json files we authored for Haunted Party Time game that are parsed by Ensemble Engine and used to manage social schema, volition formation, trigger rules, and actions

  2.5 EEGame / Build / core_libs: Contains library files developed for Haunted Party Time game, including:
    
    a) EEVisualize.js: Visualization library for building character tree, directed graphs, and undirected graphs based on Ensemble Engine Schema files
    
    b) EnsembleEngine.js: Handles communication between Unity and Ensemble in the browser as well as managing the Ensemble Engine throughout gameplay
    
    c) board.js: Contains board representation for Monte Carlo Tree Search
    
    d) ensembleInit.js: Initializes Ensemble Engine
    
    e) mcts.js: Contains Monte Carlo Tree Search functions
    
    f) mctsFunctions.js: Contains additional Monte Carlo Tree Search functions
    
    g) mctsInit.js: Initializes Monte Carlo Tree Search AI
    
    h) partytime.js: Contains functions and variables for managing game state information in browser game
    
    i) unityInit.js: Initializes Unity browser game