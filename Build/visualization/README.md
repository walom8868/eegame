# README #

### Introduction ###

In Party Time, you are a powerful ghost in an old house. Your human, self-appointed host (of course *you* are the one in charge as you've been around long before him) has decided to hold a party - how annoying! You have to take control of those guests, manipulate their actions to make them go away ASAP!

### What is this repository for? ###

This is a prototype of a social simulation game made with Ensemble Engine and Unity.

### How do I get set up? ###

Run server from project folder:
python -m SimpleHTTPServer 8000
or if using python 3: 
python -m http.server 8000

Then visit:
http://localhost:8000/PartyTime.html

### How to play ###

Every turn you will see buttons listing all possible actions your player character can do. After you press a button to take an action, every other NPC will also take an action. You can also see some descriptive text of each NPC next to his or her action buttons. Use common sense when picking actions.


### Hints ###

The graph is your firend! With your ghostly piwers, you can see the invisible social states and connections in a mysterious visualization, and use the clues to decide which actions you can force on your puppet are the most devastating(or effective, in your viewpoint).