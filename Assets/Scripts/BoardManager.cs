﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;


public class BoardManager : MonoBehaviour {

	[Serializable]
	public class Count
	{
		public int minimum;
		public int maximum;

		public Count(int min, int max){
			minimum = min;
			maximum = max;
		}
	}

	public int columns = 20;
	public int rows = 15;
	public Count wallCount = new Count(5, 9);
	public Count foodCount = new Count(1, 5);

	public GameObject exit;
	public GameObject[] floorTiles;
	public GameObject[] wallTiles;
	public GameObject[] foodTiles;
	public GameObject[] enemyTiles;
	public GameObject[] outerWallTiles;
    public GameObject[] npcs;
	public GameObject poolTable;
	public GameObject poolCue;
	public GameObject gun;
	public GameObject pill;

	public GameObject bookCase;
	public GameObject bed1;
	public GameObject nightTable1;
	public GameObject nightTable2;
	public GameObject sofa1;
	public GameObject sofa2;
	public GameObject sofaChair;
	public GameObject coffeTable;
	public GameObject tvStand;
	public GameObject desk;
	public GameObject diningTable;
	public GameObject diningChairLeft;
	public GameObject diningChairRight;
	public GameObject kitchenCounter;
	public GameObject tableWithCandle;
	public GameObject sinkAndCounter;
	public GameObject toilet;
	public GameObject bathroomSink;


	private Transform boardHolder;
	private List <Vector3> gridPositions = new List<Vector3>();
/*
	void InitialiseList(){
		gridPositions.Clear ();

		for(int x = 0; x < columns; x++){
			for (int y = 0; y < rows; y++){
				gridPositions.Add(new Vector3(x, y, 0f));
			}
		}
	}
*/
	void BoardSetup()
	{
		boardHolder = new GameObject ("Board").transform;

		for (int x = -1; x < columns + 1; x++) {
			for (int y = -1; y < rows + 1; y++) {
				GameObject toInstantiate = floorTiles [Random.Range (0, floorTiles.Length)];
				if (isWall(x, y)) {
					toInstantiate = outerWallTiles [Random.Range (0, outerWallTiles.Length)];
				} 
				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;

				instance.transform.SetParent (boardHolder);
			}
		}


	}
	bool isWall(int x, int y){
		
		bool isOutsideWall = x == -1 || x == columns || y == -1 || y == rows;
		bool isHorizontalWall = y == 7 && (x < 8 || x > 9);
		bool isHorizontalWall1 = y == 10 && x > 15;
		bool isVerticalWall = x == 7 && (y > 8 || y < 6); 
		bool isVerticalWall1 = x == 13 &&  y < 6; 
		bool isVerticalWall2 = x == 14 &&  (y > 6 && y < 11); 
		return isOutsideWall || isHorizontalWall || isHorizontalWall1 || isVerticalWall || isVerticalWall1 || isVerticalWall2;
	}

	Vector3 RandomPosition()
	{
		int randomIndex = Random.Range (0, gridPositions.Count);
		Vector3 randomPosition = gridPositions [randomIndex];
		gridPositions.RemoveAt (randomIndex);
		return randomPosition;
	}

	void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
	{
		int objectCount = Random.Range (minimum, maximum + 1);

		for (int i = 0; i < objectCount; i++) {
			Vector3 randomPosition = RandomPosition ();
			GameObject tileChoice = tileArray [Random.Range (0, tileArray.Length)];
			Instantiate (tileChoice, randomPosition, Quaternion.identity);
		}
	}
    void SetupNPCs()
    {
        Instantiate(npcs[0], new Vector3(45, 45, 0), Quaternion.identity);
        Instantiate(npcs[1], new Vector3(46, 46, 0), Quaternion.identity);
        Instantiate(npcs[2], new Vector3(50, 50, 0), Quaternion.identity);
        Instantiate(npcs[3], new Vector3(51, 51, 0), Quaternion.identity);
        Instantiate(npcs[4], new Vector3(42, 42, 0), Quaternion.identity);
    }

	public void SetupScene(){
		BoardSetup ();

        //SetupNPCs();

		//Instantiate (exit, new Vector3 (columns - 1, rows - 1, 0F), Quaternion.identity);
        
		Instantiate(poolTable, new Vector3 (17, 2, 0F), Quaternion.identity);
		Instantiate(poolCue, new Vector3 (17, 0, 0F), Quaternion.identity);
		Instantiate(bookCase, new Vector3 (17, 6, 0F), Quaternion.identity);


		Instantiate(nightTable1, new Vector3 (1, 13, 0F), Quaternion.identity);
		Instantiate(bed1, new Vector3 (3, 13, 0F), Quaternion.identity);
		Instantiate(nightTable2, new Vector3 (5, 13, 0F), Quaternion.identity);
		Instantiate(desk, new Vector3 (3, 9, 0F), Quaternion.identity);
		Instantiate(gun, new Vector3 (0, 8, 0F), Quaternion.identity);

		Instantiate(sofa1, new Vector3 (0, 2, 0F), Quaternion.identity);
		Instantiate(coffeTable, new Vector3 (3, 2, 0F), Quaternion.identity);
		Instantiate(tvStand, new Vector3 (6, 2, 0F), Quaternion.identity);
		Instantiate(sofa2, new Vector3 (3, 6, 0F), Quaternion.identity);
	

		Instantiate(kitchenCounter, new Vector3 (13, 15, 0F), Quaternion.identity);
		Instantiate(sinkAndCounter, new Vector3 (18, 14, 0F), Quaternion.identity);

		Instantiate(toilet, new Vector3 (17, 9, 0F), Quaternion.identity);
		Instantiate(bathroomSink, new Vector3 (19, 9, 0F), Quaternion.identity);
		Instantiate(pill, new Vector3 (18, 9, 0F), Quaternion.identity);

		Instantiate(diningTable, new Vector3 (11, 11, 0F), Quaternion.identity);
		Instantiate(diningChairRight, new Vector3 (10, 11, 0F), Quaternion.identity);
		Instantiate(diningChairRight, new Vector3 (10, 12, 0F), Quaternion.identity);
		Instantiate(diningChairLeft, new Vector3 (12, 11, 0F), Quaternion.identity);
		Instantiate(diningChairLeft, new Vector3 (12, 12, 0F), Quaternion.identity);
        
	}

}
