﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public float levelStartDelay = 2f;
	public float turnDelay = .1f;

	public static GameManager instance = null;

    public static Player possesedCharacter;

    public static EnsembleEngine ensembleEngine;

	public static int charsThatLeft;

    public Text remainingTurnsTextTemplate;

	public BoardManager boardScript;

	public int playerFoodPoints = 100;
	[HideInInspector] public bool playersTurn = true;

	private Text levelText;
	private GameObject levelImage;
	private List<Enemy> enemies;
	private bool enemiesMoving;
	private bool doingSetup;
    public int turns;
    public Text turnsTextInstance;



    void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);	
		}

		DontDestroyOnLoad(gameObject);
		enemies = new List<Enemy> ();

		boardScript = GetComponent<BoardManager> ();

		InitGame ();
	}

	private void OnLevelWasLoaded(int index){
		InitGame ();
	}

	void InitGame(){
		doingSetup = true;

		levelImage = GameObject.Find ("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text> ();
		levelImage.SetActive (true);
		Invoke ("HideLevelImage", levelStartDelay);

		enemies.Clear ();
		boardScript.SetupScene ();
	}

	private void HideLevelImage(){
		levelImage.SetActive (false);
		doingSetup = false;
	}

	public void GameOver(string s){
		if (GameManager.charsThatLeft == 0) {
			levelText.text = "You did not get anyone to leave the Party!";
            Application.ExternalCall("logMetric", "win_lose", "lost");
        }
		else if (GameManager.charsThatLeft == 1) {
			levelText.text = "You got " + GameManager.charsThatLeft + "\n person to leave. You still lost though";
            Application.ExternalCall("logMetric", "win_lose", "lost");
		} else {
			levelText.text = "You got " + GameManager.charsThatLeft + "\n people to leave.";
            if (charsThatLeft >= 4)
            {
                levelText.text += " You won!";
                Application.ExternalCall("logMetric", "win_lose", "won");
            }
		}

		levelImage.SetActive (true);
		enabled = false;
		EnsembleEngine ensemble = EnsembleEngine.EE;
		if (ensemble != null) {
			ensemble.cleanActionsFromUI ("");
		}
	}

	// Update is called once per frame
	void Update () {

		if (playersTurn || enemiesMoving || doingSetup) {
			return;
		}
	
		StartCoroutine (MoveEnemies ());

	}

	public void AddEnemyToList(Enemy script){
		enemies.Add (script);
	}

	IEnumerator MoveEnemies(){
		enemiesMoving = true;
		yield return new WaitForSeconds (turnDelay);

		if (enemies.Count == 0) {
			yield return new WaitForSeconds (turnDelay);
		}

		for (int i = 0; i < enemies.Count; i++) {
			enemies[i].MoveEnemy ();
			yield return new WaitForSeconds (enemies [i].moveTime);
		}

		playersTurn = true;
		enemiesMoving = false;
	}

    public void removePossession()
    {
        possesedCharacter.possesed = false;
        possesedCharacter.message.text = "Select another character to possess!";
        ensembleEngine.cleanActionsFromUI("");
        possesedCharacter = null;
    }
}
