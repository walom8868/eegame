﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public static Transform player;
    public Vector3 offset;

    public static CameraController instance = null;

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        player = GameObject.FindGameObjectWithTag("eve").transform;
        
    }

    void Update()
    {
        if (player == null) return;
        transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, offset.z); // Camera follows the player with specified offset position
    }
}