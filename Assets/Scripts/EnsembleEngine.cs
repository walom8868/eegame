﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class EnsembleEngine : MonoBehaviour {

    public static EnsembleEngine EE = null;

    public Text remainingTurns;

    public ActionButton[] buttonTemplates;
    public Text characterMessageTemplate;

    private List<ActionButton> buttons;
    private Text characterMessage;
    public GameObject canvas;

    public bool actionWithSelfInScreen;

    // Use this for initialization
    void Start () {
        if (EE == null)
        {
            EE = this;
        }
        else if (EE != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        buttons = new List<ActionButton>();

        GameManager.ensembleEngine = this;
        actionWithSelfInScreen = true;
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void cleanActionsFromUI(string s)
    {
        foreach (GameObject button in GameObject.FindGameObjectsWithTag("actionButton"))
            Destroy(button);
        buttons.Clear();
    }

    void characterLeaves(string characterAndMood)
    {
        string character = characterAndMood;
		// increment the counter of Characters that left
		GameManager.charsThatLeft++;	
        
		GameManager.possesedCharacter.message.text = "Great! " + character + " has left!";
        Destroy(GameObject.FindGameObjectWithTag(character));
    }

    void displayAvailableCharacterActions(string arg)
    {
        cleanActionsFromUI("");

        Transform canvasHolder = canvas.transform;
        Vector3 canvasBottomLeft = new Vector3(100, 50, 0);
        Rect canvasRect = canvas.GetComponent<RectTransform>().rect;

        float previousButtonYTop = canvasBottomLeft.y;

        string allActions = arg.Split('=')[0];
        string responder = arg.Split('=')[1];

        foreach (string action in allActions.Split('-'))
        {
            // Create a button for each action
            ActionButton instance = Instantiate(buttonTemplates[0], canvasBottomLeft, Quaternion.identity) as ActionButton;
            if (instance == null) Debug.Log("Instance is null");
            instance.transform.SetParent(canvasHolder);

            // Set the actions real and display name in the button
            instance.actionDisplayName = action.Split(':')[0];
            instance.actionRealName = action.Split(':')[1];

            // Set its position
            // If this is the first button, set at 0, 0
            if (buttons.Count == 0)
                instance.transform.position = canvasBottomLeft;

            // Otherwise, set it right on top of the previous one
            else
            {
                previousButtonYTop = buttons[buttons.Count - 1].transform.position.y;
                instance.transform.position = new Vector3(canvasBottomLeft.x, previousButtonYTop + 30, 0);
            }

            // Add it to the list of buttons
            buttons.Add(instance);
        }

        // Add message of who are we talking with
        Text charMessageInstance = Instantiate(characterMessageTemplate, new Vector3(canvasBottomLeft.x, previousButtonYTop + 55, 0), Quaternion.identity) as Text;
        charMessageInstance.transform.SetParent(canvasHolder);
        charMessageInstance.text = "Actions with: " + responder;
    }
}