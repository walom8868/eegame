﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {
    
	public float restartLevelDelay = 1f;
	public Text message;

	public AudioClip moveSound1;
	public AudioClip moveSound2;
	public AudioClip eatSound1;								
	public AudioClip gameOverSound;

	private Animator animator;
	private int food;

    public bool possesed;

	// Use this for initialization
	protected override void Start () {
		animator = GetComponent<Animator> ();
		food = GameManager.instance.playerFoodPoints;    
		animator.SetInteger("Direction", 2);
        // All characters start as non-possesed
        possesed = false;


		base.Start ();
	}

	private void OnDisable(){
		GameManager.instance.playerFoodPoints = food;
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.playersTurn)
			return;

        // If not possesed, no control over this character
        if (!possesed)
            return;

		int horizontal = 0;
		int vertical = 0;


		horizontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		if (horizontal != 0)
			vertical = 0;

        if (horizontal != 0 || vertical != 0)
        {
            if (horizontal > 0) animator.SetInteger("Direction", 1);
            else if (horizontal < 0) animator.SetInteger("Direction", 3);
            else if (vertical > 0) animator.SetInteger("Direction", 0);
            else if (vertical < 0) animator.SetInteger("Direction", 2);

            AttemptMove<ActionableObject>(horizontal, vertical);
        }

	}

	protected override void AttemptMove <T> (int xDir, int yDir){
		base.AttemptMove <T> (xDir, yDir);

		RaycastHit2D hit;

		if (Move (xDir, yDir, out hit)) {
			SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
		}

		CheckIfGameOver ();

		GameManager.instance.playersTurn = false;

	}

	private void OnTriggerEnter2D(Collider2D other){
			
		if(other.tag == "InventoryItem")
		{
			//Add pointsPerFood to the players current food total.
			//food += pointsPerFood;

			//Update foodText to represent current total and notify player that they gained points
			//foodText.text = "+" + pointsPerFood + " Food: " + food;

			//Call the RandomizeSfx function of SoundManager and pass in two eating sounds to choose between to play the eating sound effect.
			SoundManager.instance.RandomizeSfx (eatSound1);

			//Disable the food object the player collided with.
			other.gameObject.SetActive (false);

			string nameToSend = other.name;
			int endIndex = nameToSend.IndexOf ("(");
			nameToSend = nameToSend.Substring (0, endIndex).ToLower();
			Application.ExternalCall("addItemToCharacterInventory", this.tag, nameToSend);
		}
		/*
		//Check if the tag of the trigger collided with is Soda.
		else if(other.tag == "InventoryItem")
		{
			//Add pointsPerSoda to players food points total
			food += pointsPerSoda;

			//Update foodText to represent current total and notify player that they gained points
			foodText.text = "+" + pointsPerSoda + " Food: " + food;

			//Call the RandomizeSfx function of SoundManager and pass in two drinking sounds to choose between to play the drinking sound effect.
			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);

			//Disable the soda object the player collided with.
			other.gameObject.SetActive (false);
		}

		*/



	}

	protected override void OnCantMove <T> (T component){
		ActionableObject other = component as ActionableObject;
        Application.ExternalCall("getAvailableCharacterActions", this.tag, other.tag);
        GameManager.ensembleEngine.actionWithSelfInScreen = false;

	}

	private void Restart(){
		Scene activeScene = SceneManager.GetActiveScene ();

		SceneManager.LoadScene (activeScene.name);
	}

	private void CheckIfGameOver(){
		if (GameManager.instance.turns <= 0) {
			SoundManager.instance.PlaySingle (gameOverSound);
			SoundManager.instance.musicSource.Stop ();
			GameManager.instance.GameOver ();
		}
	}

    public void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (GameManager.possesedCharacter != null)
                return;
             
            possesed = true;
            GameManager.possesedCharacter = this;
            CameraController.player = this.transform;
            message.text = "Currently possessing: " + this.tag;

            // Get actions to do with self
            Application.ExternalCall("getAvailableCharacterActions", this.tag, this.tag);

            GameManager.ensembleEngine.actionWithSelfInScreen = true;

            // Log this event
            Application.ExternalCall("logMetric", "possessed", this.tag);
        }
    }
}
