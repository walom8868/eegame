﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActionButton : MonoBehaviour {

    public string actionRealName;
    public string actionDisplayName;

    public void applyAction()
    {
        Application.ExternalCall("applyAction", actionRealName);
        GameManager.ensembleEngine.cleanActionsFromUI("");
        GameManager.instance.turns--;
        GameManager.ensembleEngine.remainingTurns.text = "Remaining Actions: " + GameManager.instance.turns;
    }

    // Use this for initialization
    void Start () {
        // Set display action name
        GetComponentInChildren<Text>().text = actionDisplayName;

        // Add the onClick listener to apply the action
        GetComponent<Button>().onClick.AddListener(applyAction);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
